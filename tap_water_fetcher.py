from common import read_page, get_url
import json

search_url = "https://www.ewg.org/tapwater/state.php?stab=CA"

page_content = read_page(search_url)

get_more_url = get_url(page_content, "More")
print('More Button url is : ',get_more_url )

#output array
data = []

#scrapping table data
utility_table = page_content.find('table',{'class':'utility-violation-table'})

all_rows= utility_table.find_all('tr')
i = 0
for rows in all_rows:
    res = {}
    utility = rows.find('td',{'data-label':'Utility'})
    if utility != None:
        res['utility'] = utility.text.strip()
    location = rows.find('td',{'data-label':'Location'})
    if location != None:
        res['location'] = location.text.strip()
    population = rows.find('td',{'data-label':'Population'})  
    if population != None:
        res['population'] = population.text.strip()

    data.append(res)

print(data)
with open('data.txt', 'w') as outfile:
        json.dump(data, outfile)
    
    
