from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup


def read_page(url):
    #open connection and grab the page
    url = Request(url,headers = {"User-Agent": "Mozilla/5.0"})
    urlClient = urlopen(url)
    page_html = urlClient.read()
    urlClient.close()

    #html parsing
    page_soup = soup(page_html, "html.parser")
    return page_soup

#find more button url
def get_url(html_content,word):
    links = html_content.find_all('a')
    for link in links:
        button_name = link.text.strip()
        if button_name == word:
            return link.get('href')
            print ('More buuton url : ',link.get('href'))
