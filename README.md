# Web Scrapper

Web scrapper for https://www.ewg.org/tapwater/state.php?stab=CA from python

**Git clone and set up**

* git clone https://gitlab.com/Ruchi_sujanthi/web-scrapper.git
* cd my-project
* python3 tap_water_fetcher.py

**Install and run**

Pre requisites:
 
* Python3 install 


**To run the CLI:**

*  python3 <file_name>.py (eg : python3 tap_water_fetcher.py)



**Output**

* script return the more button url and all the data in write in csv file

